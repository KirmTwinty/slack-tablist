;;; slack-tablist.el --- Provides an interface to slack.el

;; Author: Thibaud Toullier
;; Maintainer: Thibaud Toullier
;; Version: 0.1
;; Package-Requires: ((slack) (transient))
;; URL: https://gitlab.com/KirmTwinty/slack-tablist
;; Homepage: https://gitlab.com/KirmTwinty/slack-tablist
;; Keywords: slack-tablist, slack, transient, tabulated-list

;; This file is not part of GNU Emacs

;; This file is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation; either version 3, or (at your option)
;; any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; For a full copy of the GNU General Public License
;; see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; This program is an interface to the slack.el for showing room lists
;; as `tabulated-list-mode'.
;; You can visit the homepage at URL `https://gitlab.com/KirmTwinty/slack-tablist'
;; for more details.

;;; Code:
(require 'slack)
(require 'tabulated-list)
(require 'transient)

(define-derived-mode slack-tablist-mode tabulated-list-mode "slack-tablist-mode"
  "Major mode for tabulated list example."
  (setq tabulated-list-format [("Group" 16 nil)
			       ("" 2 nil)
			       ("Count" 5 nil)
			       ("Name" 30 t)
			       ("Created" 10 t)
			       ("Status" 0 nil)
			       ]);; last columnt takes what left
  (add-hook 'tabulated-list-revert-hook #'slack-tablist--refresh nil t)
  (setq tabulated-list-padding 4)
  (tabulated-list-init-header)
  (slack-tablist--refresh)
  (tabulated-list-print t))

(defvar
  slack-tablist-list-entries
  nil
  "Fetched entries from slack server.")

(defclass slack-tablist-header ()
  ((value :initarg :value :initform "")))

(cl-defmethod slack-tablist-user-status (room team)
  (slack-user-status (oref room id) team))

(defun slack-tablist--refresh ()
  "Override default slack-select-rooms."
  (interactive)
  (let* ((team (slack-team-select)) ;; still ask the team
	 (ims (cl-loop for team in (list team) ;
                    append (append
			    (slack-team-ims team))))
	 (groups (cl-loop for team in (list team) ;
                       append (append
                               (slack-team-groups team))))
	 (channels (cl-loop for team in (list team) ;
                       append (append
                               (slack-team-channels team))))
	 (entries (append
		   (list (slack-tablist-header :value "Instant messages"))
		   (slack-room-names
		    ims team #'(lambda (rs) (cl-remove-if #'slack-room-hidden-p rs)))
		   (list (slack-tablist-header :value "Groups ---------"))
		   (slack-room-names
		    groups team #'(lambda (rs) (cl-remove-if #'slack-room-hidden-p rs)))
		   (list (slack-tablist-header :value "Channels -------"))
		   (slack-room-names
		    channels team #'(lambda (rs) (cl-remove-if #'slack-room-hidden-p rs))))))
    (setq slack-tablist-list-entries entries)
    (tabulated-list-init-header)
    (setf tabulated-list-entries
	  (let ((index -1))
	    (mapcar #'(lambda (x)
		      (cl-incf index)
		      (if (typep x slack-tablist-header)
			  (list index (vector
				       (oref x :value)
				       "--"
				       "-----"
				       "------------------------------"
				       "----------"
				       ""
				       ))
			(let* ((label (car x))
			       (room (cdr x)))
			  (list index (vector
				       ""
				       (slack-room-label-prefix room team)
				       (slack-room-mention-count-display room team)
				       (slack-room-name room team)
				       (slack-format-ts
					(number-to-string (oref room created)) "%Y-%m-%d")
				       (slack-tablist-user-status room team)))))) entries)))))

(defun slack-tablist-mode-open ()
  "Open conversation under cursor."
  (interactive)
  (when (tabulated-list-get-id)
    (unless (typep (nth (tabulated-list-get-id) slack-tablist-list-entries) slack-tablist-header)
      (let ((room (cdr (nth (tabulated-list-get-id) slack-tablist-list-entries))))
	(slack-room-display room (slack-team-select))))))

;; mode map and transient
(defvar
  slack-tablist-mode-map
  nil
  "Keymap for slack-tablist-mode.")

(setq slack-tablist-mode-map (make-sparse-keymap))

(define-key slack-tablist-mode-map (kbd "?") 'slack-tablist-mode-help)
(define-key slack-tablist-mode-map (kbd "o") 'slack-tablist-mode-open)
(define-key slack-tablist-mode-map (kbd "<return>") 'slack-tablist-mode-open)
(define-key slack-tablist-mode-map (kbd "RET") 'slack-tablist-mode-open)

(transient-define-prefix slack-tablist-mode-help ()
  "Help transient for slack-tablist mode."
  ["Slack-Tablist mode help"
   ("o" "Open" slack-tablist-mode-open)
   ("RET" "Open" slack-tablist-mode-open)
   ("<return>" "Open" slack-tablist-mode-open)
   ])

;;;###autoload
(defun slack-tablist (&rest _ignore)
  "Show buffer listing joined rooms.
Calls `pop-to-buffer-same-window'.  Interactively, with prefix,
call `pop-to-buffer'."
  (interactive)
  (with-current-buffer (get-buffer-create "*Slack-Tablist Rooms*")
    (slack-tablist-mode)
    (funcall (if current-prefix-arg
                 #'pop-to-buffer #'pop-to-buffer-same-window)
             (current-buffer))))

(provide 'slack-tablist)
;;; slack-tablist.el ends here
